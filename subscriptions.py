def get_subscriptions(subscriptions_xml_filename):
    import xml.etree.ElementTree as XTree

    root = XTree.parse(subscriptions_xml_filename)

    res = {}
    for e in root.findall('.//outline/outline'):
        assert 'xmlUrl' in e.keys()
        assert 'title' in e.keys()
        assert 'htmlUrl' in e.keys()
        res[e.get('xmlUrl')] = {
                'title': e.get('title'),
                'htmlUrl': e.get('htmlUrl')
        }

    return res


def list_subscriptions(subscriptions_xml_filename):
    s = get_subscriptions(subscriptions_xml_filename)
    for url, info in s.iteritems():
        print '[%s (%s)] %s' % (info['title'], info['htmlUrl'], url)


# List of 'xmlUrl's to ignore. Subscriptions whose 'xmlUrl' attribute is in this
# list won't be returned.
STARRED_IGNORE  = [
    # 'http://starred.subscription.i.actually.dont.care.about/',
    # ...
]

def get_starred(starred_json_filename):
    import json

    starred = json.load(file(starred_json_filename))
    assert 'items' in starred
    res = {}
    for e in starred['items']:
        assert 'origin' in e
        assert 'published' in e
        assert 'streamId' in e['origin']
        assert 'title' in e['origin']
        assert 'htmlUrl' in e['origin']
        streamId = e['origin']['streamId']
        assert streamId.startswith('feed/')
        xmlUrl = streamId[len('feed/'):]
        title = e['origin']['title']
        htmlUrl = e['origin']['htmlUrl']
        published = int(e['published'])
        if xmlUrl not in STARRED_IGNORE:
            if xmlUrl in res:
                assert title == res[xmlUrl]['title']
                assert htmlUrl == res[xmlUrl]['htmlUrl']
                res[xmlUrl]['starred_count'] += 1
                if published < res[xmlUrl]['starred_oldest']:
                    res[xmlUrl]['starred_oldest'] = published
            else:
                res[xmlUrl] = {
                        'title': title,
                        'htmlUrl': htmlUrl,
                        'starred_count': 1,
                        'starred_oldest': published
                }
    return res


#list_subscriptions('subscriptions.xml')
All = get_subscriptions('subscriptions.xml')
AllSet = set(All.keys())
Starred = get_starred('starred.json')
StarredSet = set(Starred.keys())

assert StarredSet <= AllSet


# Set of 'xmlUrl's that will certainly be backed up. (Note that this is not meant to be
# a complete set, other subscriptions may also be backed up.)
DoBackup = set([
    # 'http://subscription.i.certainly.want.to.back.up/',
    # ...
])

# Set of 'xmlUrl's that definitely won't be backed up.
DontBackup = set([
    # 'http://subscription.i.dont.want.to.back.up/',
    # ...
])


assert DoBackup <= AllSet
assert DontBackup <= AllSet
assert DoBackup.intersection(DontBackup) == set()


BackupExtra = 500

if __name__ == '__main__':
    print 'All subscriptions:', len(All)
    print 'Subscriptions with starred items:', len(Starred)

    # What to backup how:
    oldest_plus_extra = len(StarredSet - DontBackup)
    latest_extra = len(AllSet - StarredSet - DontBackup)
    assert len(All) == oldest_plus_extra + latest_extra + len(DontBackup)

    print 'Backup to oldest starred plus %d:' % BackupExtra, oldest_plus_extra
    print 'Backup latest %d:' % BackupExtra, latest_extra
    print 'Total to backup:', oldest_plus_extra + latest_extra
