# encoding: utf-8

# <<EncodeFix
# The program could try to output unicode characters outside ascii. The
# following should prevent a UnicodeEncodeError.
# Taken from http://stackoverflow.com/questions/4545661/unicodedecodeerror-when-redirecting-to-file
import codecs
import locale
import sys
# Wrap sys.stdout into a StreamWriter to allow writing unicode.
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)
# EncodeFix>>

import os
import os.path
import cookielib
import urllib
import json
import hashlib
import subscriptions as subs
from web import web  # from pyrfeed


URI_LOGIN = 'https://www.google.com/accounts/ClientLogin'
URI_READER = 'http://www.google.com/reader/'
HTTP_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
LOGIN = ''
PASSWD = ''

ITEMS_PER_REQUEST = subs.BackupExtra


class Client(object):
    def __init__(self, http_agent=HTTP_AGENT):
        self._agent = http_agent
        self._sid = None
        self._client = web(agent=self._agent)

    def login(self, login, passwd):
        query = {
                'service': 'reader',
                'Email': login,
                'Passwd': passwd,
                'source': self._agent,
                'continue': 'http://www.google.com/'
        }

        sidinfo = self._client.get(URI_LOGIN, query)

        SID_ID = 'SID='
        if SID_ID in sidinfo:
            pos_beg = sidinfo.find(SID_ID)
            pos_end = sidinfo.find('\n', pos_beg)
            self._sid = sidinfo[pos_beg + len(SID_ID):pos_end]

        if self._sid != None:
            cookie = cookielib.Cookie(version=0, name='SID', value=self._sid,
                    port=None, port_specified=False, domain='.google.com',
                    domain_specified=True, domain_initial_dot=True, path='/',
                    path_specified=True, secure=False, expires='1600000000',
                    discard=False, comment=None, comment_url=None, rest={})
            self._client.cookies().set_cookie(cookie)
            return True

    def get(self, abspath):
        url = URI_READER + abspath

        return self._client.get(url)

    def get_user_feed(self, feed_url, **kwargs):
        """
        Get the raw JSON corresponding to feed 'feed_url'. Append the entries
        in 'kwargs' as parameter=value query pairs.
        """
        assert feed_url

        url = 'api/0/stream/contents/feed/' + urllib.quote(feed_url, safe='')
        query_started = False
        for qpair in kwargs.iteritems():
            if not query_started:
                url += '?'
                query_started = True
            else:
                url += '&'
            url += '%s=%s' % qpair

        return self.get(url)

    def backup_until_and_then_n(self, feed_info, until, n):
        """
        Backup all items from the subscription referred to by 'feed_info' up
        to the item with 'published' timestamp equal to 'until' plus the
        following 'n' items.
        Backed up data is stored inside a separate directory.
        """
        assert 'xmlUrl' in feed_info
        assert 'title' in feed_info
        assert 'htmlUrl' in feed_info

        feed_dirname = create_feed_dir(feed_info)

        until_done = False
        n_done = 0
        if until == None:
            until_done = True

        print 'Backing up %s (%s)' % (feed_info['xmlUrl'], feed_info['title'])

        requests = 0
        continuation = None
        while not until_done or n_done < n:
            print 'Sending request %s' % requests
            if not continuation:
                response = self.get_user_feed(feed_info['xmlUrl'],
                        n=ITEMS_PER_REQUEST - n_done)
            else:
                response = self.get_user_feed(feed_info['xmlUrl'],
                        n=ITEMS_PER_REQUEST - n_done, c=continuation)

            feed_obj = json.loads(response)
            assert type(feed_obj) is dict

            if not until_done:
                p = process_feed_part(feed_obj, until, n - n_done)
                until_done = p[0] != None
            else:
                p = process_feed_part(feed_obj, None, n - n_done)
            n_done += p[1]

            write_feed_data(feed_dirname, requests, feed_obj)
            requests += 1
            if 'continuation' in feed_obj:
                continuation = feed_obj['continuation']
            else:
                if not until_done:
                    print 'Error: Could not found "until" and no more ' \
                            'continuation found.'
                if n_done < n:
                    print 'Warning: Only %s items backed up after "until" ' \
                            'and no more continuation found.' % n_done
                break

        print 'Done'


def write_feed_data(dirname, index, feed):
    """
    Write feed data 'feed' (with index 'index') inside directory 'dirname'.
    """
    filename = os.path.join(dirname, str(index) + '.json')
    assert not os.path.exists(filename)
    print 'Creating file %s' % filename
    f = file(filename, 'w+')
    json.dump(feed, f)


def process_feed_part(feed, until, n):
    """
    Process 'feed' according to 'until' (timestamp of oldest item which must
    be backed up) and 'n' (amount of items after 'until' which must be backed
    up).
    Assume "feed['items']" is sorted descendently by 'published' timestamp.
    Advance through it looking for an item with 'published' timestamp equal to
    'until'. Then continue past the next 'n' items. Keep all those items and
    remove the rest.
    If 'until' is None, assume such item was already found before looking at
    the first item in "feed['items']". This has the effect of keeping the
    first 'n' items from "feed['items']'.
    Return (at, k) where 'at' is the index in "feed['items']" at which 'until'
    was found (or None, if it wasn't found) and 'k' is the amount of items
    past 'until' that were preserved.
    """
    until_at = None
    prev_published = None
    if until == None:  # hack: 'until' was reached already, we now care only for 'n'
        until_at = -1
    else:
        for i in xrange(0, len(feed['items'])):
            # workaround: items are often but not always ordered by 'published'
            #crawl_time = feed['items'][i]['crawlTimeMsec']
            #crawl_time = int(crawl_time[0:-3])  # remove msec part
            #published = min(int(feed['items'][i]['published']), crawl_time)
            #print '%s %s' % (prev_published, published)
            published = int(feed['items'][i]['published'])
            if prev_published:
            #    assert prev_published >= published  # order is descendent
                if prev_published < published:
                    print 'Warning: Feed is not sorted by "published".'
            #        print feed['items'][i]
            if published == until:
                print 'Found "until" entry.'
                until_at = i
                break
            elif published < until:
                print 'Warning: Found entry with "published" < "until" ' \
                        '(%s < %s).' % (published, until)
                until_at = i
                break
            prev_published = published

    if until_at != None:
        need_slice = len(feed['items']) > until_at + n + 1
        if need_slice:
            feed['items'] = feed['items'][0:until_at + n + 1]
        n_more = len(feed['items']) - until_at - 1
    else:
        n_more = 0

    return (until_at, n_more)


def create_feed_dir(feed_info):
    """
    Create a directory for the feed represented in 'feed_info'.
    Create also a file inside that directory with the information in
    'feed_info'.
    Return the directory name.
    """
    assert 'xmlUrl' in feed_info

    feed_md5 = hashlib.md5(feed_info['xmlUrl']).hexdigest()
    if not os.path.isdir(feed_md5):
        assert not os.path.exists(feed_md5)
        os.mkdir(feed_md5)
    info_filename = os.path.join(feed_md5, 'info')
    if not os.path.isfile(info_filename):
        assert not os.path.exists(info_filename)
        f = file(info_filename, 'w+')
        json.dump(feed_info, f)

    return feed_md5


if __name__ == '__main__':
    c = Client()
    logged_in = c.login(LOGIN, PASSWD)
    assert logged_in

    i = 0
    for k in subs.StarredSet - subs.DontBackup:
        s = {'xmlUrl': k}
        oldest_starred = subs.Starred[k]['starred_oldest']
        s.update(subs.All[k])
        c.backup_until_and_then_n(s, oldest_starred, subs.BackupExtra)
        i += 1

    for k in subs.AllSet - subs.StarredSet - subs.DontBackup:
        s = {'xmlUrl': k}
        s.update(subs.All[k])
        c.backup_until_and_then_n(s, None, subs.BackupExtra)
        i += 1

    print 'Backed up %s subscriptions.' % i
